import React, { useState } from 'react';
import shiftsD from '../datasource/shifts.json';
import roles from '../datasource/roles.json';
import employees from '../datasource/employees.json';
import config from '../datasource/config.json';

const AppContext = React.createContext({});

export const AppProvider = ({children}) => {
  
  const [viewType, setViewType] = useState('table');
  const [shifts, setShifts] = useState(shiftsD);
  
  const getEmployeeData = (empId) => {
    return employees.find(item=>{return item.id===empId});
  }

  const getRole = (roleId) => {
    return roles.find(item=>{
      return item.id===roleId
    });
  }

  return (<AppContext.Provider value={{
    viewType,
    setViewType,
    shifts,
    setShifts,
    employees,
    config,
    roles,
    getEmployeeData,
    getRole,
  }}
  >
    {children}
  </AppContext.Provider>);
}

export default AppContext;