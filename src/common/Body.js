
import React from 'react';
import {
  Table
} from 'semantic-ui-react';

const Body = ({data}) => {
  
  const headers = Object.keys(data[0]);

  return (
    <Table.Body>
      {data.map((item, index)=>{
        return <Table.Row key={`row-${index}`}>
        {headers.map(cell=>{
          return <Table.Cell key={`bodycell-${cell}`}>{item[cell]}</Table.Cell>
        })}
      </Table.Row>
      })}
    </Table.Body>
  );
}

export default Body;