import React from 'react';
import {
  Table
} from 'semantic-ui-react';

const Header = ({object=null}) => {
  
  if(!object) return null;
  const headers = Object.keys(object);
  return (
    <Table.Header>
      <Table.Row>
      {headers.map(item=>{
        return <Table.HeaderCell key={item}>{item}</Table.HeaderCell>
      })}
    </Table.Row>
    </Table.Header>
  );
}

export default Header;