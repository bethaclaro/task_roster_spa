import React, { useState, useEffect } from 'react';

import {Modal, Card, Form, Button} from 'semantic-ui-react';
import {
  TimeInput,
} from 'semantic-ui-calendar-react';
import moment from 'moment';

function EditModal({modalOpen, selectedEvent, updateShift, parentDispatch}) {

  const [startTime, setStartTime] = useState("");
  const [endTime, setEndTime] = useState("");

  useEffect(()=>{
    setStartTime(moment(selectedEvent.start_time).format('hh:mm A').toString());
    setEndTime(moment(selectedEvent.end_time).format('hh:mm A').toString());
  }, [selectedEvent]);


  const handleStartChanged = (event, {value}) => {
    setStartTime(value);
  };

  const handleEndChanged = (event, {value}) => {
    setEndTime(value);
  };

  const onClose = () => {
    parentDispatch({type: 'TOGGLE_MODAL', payload: false});
  }

  const constructDate = (oldVal, newTime) => {
    let temp = `${moment(oldVal).format("YYYY-MM-DD").toString()} ${newTime}`;
    return moment(new Date(temp));
  };

  const onSubmit = () => {
    updateShift({
      id: selectedEvent.id,
      newstart: constructDate(selectedEvent.start_time, startTime ),
      newend: constructDate(selectedEvent.end_time, endTime),
    });
    parentDispatch({type: 'TOGGLE_MODAL', payload: false});
  }
  
  return <Modal className="edit-modal" open={modalOpen} size='mini' onClose={onClose}>
    <Modal.Content>
      <Card>
        <Card.Content header={selectedEvent.title} />
        <Card.Content className="content2">
          <Form onSubmit={onSubmit}>
            <Form.Field>
              <label>Employee ID</label>
              <input value={selectedEvent.employee_id} disabled />
            </Form.Field>
            <Form.Field>
              <label>Role</label>
              <input value={selectedEvent.role} disabled />
            </Form.Field>
            <Form.Field className="field-starttime">
              <label>Start Time</label>
              <TimeInput
                name="startTime"
                iconPosition="left"
                timeFormat="AMPM"
                value={startTime}
                onChange={handleStartChanged}
                popupPosition="bottom right"
                animation="fade"
              />
            </Form.Field>
            <Form.Field>
              <label>End Time</label>
              <TimeInput
                timeFormat="AMPM"
                name="endTime"
                iconPosition="left"
                value={endTime}
                onChange={handleEndChanged}
                popupPosition="bottom right"
                animation="fade"
              />
            </Form.Field>
            <Button type='submit'>Save Changes</Button>
          </Form>
        </Card.Content>
      </Card>
    </Modal.Content>
  </Modal>;
}

export default EditModal;