import React, {useContext} from 'react';
import AppContext from '../../contexts/AppContext';

import {Table} from 'semantic-ui-react';
import Header from '../../common/Header';
import Body from '../../common/Body';

import moment from 'moment-timezone';

const TableView = ()=> {
  const {shifts, getEmployeeData, getRole, config} = useContext(AppContext);


  const getHeaders = () => {
    const temp = {
      employee: '',
      start_time: '',
      end_time: '',
      role: '',
      break_duration: '',
    };
    return temp;
  }

  const preProcessData = (data) => {
    const temp = data.map(item=>{
      const employee = getEmployeeData(item.employee_id);
      const roleD = getRole(item.role_id);
      return {
        employee: `${employee.first_name} ${employee.last_name}`,
        start_time: moment.tz(item.start_time, config.timeZone).format('YYYY-MM-DD HH:mm').toString(),
        end_time: moment.tz(item.end_time, config.timeZone).format('YYYY-MM-DD HH:mm').toString(),
        role: roleD.name,
        break_duration: item.break_duration,
      };
    })
    return temp;
  }

  const tableData = preProcessData(shifts);

  return (
    <div>
      <Table striped>
        <Header object={getHeaders()} />
        <Body data={tableData} />
      </Table>
    </div>
  );
}

export default TableView;