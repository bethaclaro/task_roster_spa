import React, { useContext } from 'react';
import AppContext from '../../contexts/AppContext';

import {
  Table
} from 'semantic-ui-react';

import Header from '../../common/Header';
import Body from '../../common/Body';

const TableView = () => {

  const {employees, shifts, roles} = useContext(AppContext);

  return (
    <div>
      <div>Employees</div>
      <Table striped>
        <Header object={employees[0]} />
        <Body data={employees} />
      </Table>

      <div>Shifts</div>
      <Table striped>
        <Header object={shifts[0]} />
        <Body data={shifts} />
      </Table>

      <div>Roles</div>
      <Table striped>
        <Header object={roles[0]} />
        <Body data={roles} />
      </Table>

    </div>

  );
}

export default TableView;