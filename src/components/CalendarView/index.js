import React, { useContext, useReducer } from 'react';

import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import momentTimezonePlugin from '@fullcalendar/moment-timezone';
import interactionPlugin from '@fullcalendar/interaction';
import moment from 'moment-timezone';
import EditModal from '../../common/EditModal';

import AppContext from '../../contexts/AppContext';

import './calendarview.css';

const CalendarView = () => {
  const {shifts, config, getEmployeeData, getRole, 
    setShifts,
  } = useContext(AppContext);

  const initialData = {
    modalOpen: false,
    selectedEvent: {},
  };

  const reducer = (state, action) => {
    if(action.type==='TOGGLE_MODAL') {
      return {...state, modalOpen: action.payload};
    }
    if(action.type==='SET_SELECTED') {
      return {...state, selectedEvent: action.payload};
    }
  }

  const [calendarData, dispatch] = useReducer(reducer, initialData);

  const preProcessData = (data) => {
    const temp = data.map(item=>{
      const employee = getEmployeeData(item.employee_id);
      const role = getRole(item.role_id);
      return {
        title: `${employee.first_name} ${employee.last_name}`,
        start: moment.tz(item.start_time, config.timezone).format(),
        end: moment.tz(item.end_time, config.timezone).format(),
        backgroundColor: role.background_colour,
        textColor: 'black',
        extendedProps: {...item},
      }
    })
    return temp;
  };

  const handleEventClick = info => {
    const role = getRole(info.event.extendedProps.role_id);
    const temp = {
      ...info.event.extendedProps, 
      start_time: info.event.start,
      end_time: info.event.end,
      title: info.event.title,
      role: role.name
    };
    dispatch({type: 'SET_SELECTED', payload: temp});
    dispatch({type: 'TOGGLE_MODAL', payload: true});
  }

  const eventData = preProcessData(shifts);

  const updateShift = (shift) => {
    const idx = shifts.findIndex(item=>{return item.id===shift.id});

    const updatedShifts = [...shifts];
    
    updatedShifts[idx] = {...shifts[idx],
      start_time: moment.tz(shift.newstart, "UTC").format(),
      end_time: moment.tz(shift.newend, "UTC").format(),
    };
    
    setShifts(updatedShifts);
  };

  return (
    <React.Fragment>
      <EditModal
        modalOpen={calendarData.modalOpen}
        parentDispatch={dispatch}
        selectedEvent={calendarData.selectedEvent}
        updateShift={updateShift}
      />
      <div className="calendar-wrapper">
        <FullCalendar
          dayCellClassNames="day-cell"
          plugins={[dayGridPlugin, momentTimezonePlugin, interactionPlugin]}
          initialView="dayGridWeek"
          events={eventData}
          initialDate="2018-06-17"
          timeZone={config.timezone}
          eventClick={handleEventClick}
          height={600}
          />
        </div>
    </React.Fragment>
  );
}

export default CalendarView;
