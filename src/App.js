import React, { useContext } from 'react';
import './App.css';
import AppContext from './contexts/AppContext';

import {Button} from 'semantic-ui-react';
import Raw from './components/Raw';
import TableView from './components/TableView';
import CalendarView from './components/CalendarView';

function App() {
  const { viewType, setViewType } = useContext(AppContext);

  const onClickHandler = type => {
    setViewType(type);
  };

  const isDisabled = type => {
    if(viewType===type) return true;
    return false;
  };

  return (
    <div className="App">
      <div className="wrapper">
      <Button primary
        onClick={()=>{onClickHandler('raw')}}
        disabled={isDisabled('raw')}
        >Raw</Button>
      <Button primary
        onClick={()=>{onClickHandler('table')}}
        disabled={isDisabled('table')}
      >Table</Button>
      <Button primary
        onClick={()=>{onClickHandler('calendar')}}
        disabled={isDisabled('calendar')}
      >Calendar</Button>
      {viewType==='raw' && <Raw />}
      {viewType==='calendar' && <CalendarView />}
      {viewType==='table' && <TableView />}
      </div>
    </div>
  );
}

export default App;
